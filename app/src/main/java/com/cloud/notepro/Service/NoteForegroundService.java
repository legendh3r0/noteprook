package com.cloud.notepro.Service;

import android.accessibilityservice.AccessibilityService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Animation;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.cloud.notepro.BroadcastNotify;
import com.cloud.notepro.Helper.DrawSupport;
import com.cloud.notepro.MainActivity;
import com.cloud.notepro.Net.MyUrl;
import com.cloud.notepro.R;

public class NoteForegroundService extends AccessibilityService {

    private static final String TAG = NoteForegroundService.class.getSimpleName();
    IntentFilter mFilter;
    WindowManager mWindowManager;
    Animation mAnimation;
    private static final int NOTIFICATION_ID = 2999;
    private static final String CHANNEL_ID = "MyForegroundService_ID";
    private static final CharSequence CHANNEL_NAME = "MyForegroundService Channel";
    private final IBinder mBinder = new LocalBinder();
    TextView txtClose;

    public class LocalBinder extends Binder {
        public NoteForegroundService getService() {
            return NoteForegroundService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate ");
        Toast.makeText(this, "The service is running", Toast.LENGTH_SHORT).show();
        mFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        mFilter.addAction(Intent.ACTION_SCREEN_ON);
        mFilter.addAction(Intent.ACTION_SCREEN_OFF);
        mFilter.addAction(Intent.ACTION_USER_PRESENT);
        startForeground(NOTIFICATION_ID, createNotification("The service is running"));
    }

    private Notification createNotification(String message) {

        // Get the layouts to use in the custom notification
        RemoteViews notificationLayout = new RemoteViews(getPackageName(), R.layout.view_foregound_service);
        NotificationManager mNotificationManager;
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        Intent notificationIntent = new Intent(this, BroadcastNotify.class);
        notificationIntent.setAction("action-go");
        sendBroadcast(notificationIntent);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 125, notificationIntent, 0);
        notificationLayout.setOnClickPendingIntent(R.id.button, pendingIntent);
        Bitmap payableLogo = BitmapFactory.decodeResource(getResources(), R.drawable.ic_pen);

        mBuilder.setContentTitle("My Service")
                .setContent(notificationLayout)
                .setContentText(message)
                .setPriority(Notification.PRIORITY_HIGH)
                .setLargeIcon(payableLogo)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentIntent(pendingIntent)
                .setAutoCancel(false)
                .setCustomBigContentView(notificationLayout);

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = CHANNEL_ID;
            NotificationChannel channel = new NotificationChannel(channelId, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);
        }

        return mBuilder.build();
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {

    }

    @Override
    public void onInterrupt() {

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        registerOverlayReceiver();
        return Service.START_STICKY;
    }

    private void showDialog(String aTitle) {
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);

    }


    private void hideDialog() {

    }

    @Override
    public void onDestroy() {
        unregisterOverlayReceiver();
        super.onDestroy();
    }

    private void registerOverlayReceiver() {
        registerReceiver(overlayReceiver, mFilter);
    }

    private void unregisterOverlayReceiver() {
        hideDialog();
        registerReceiver(overlayReceiver, mFilter);
    }


    private BroadcastReceiver overlayReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("quansp", "[onReceive]" + action);
            if (action.equals(Intent.ACTION_SCREEN_ON)) {
//                showDialog("Esto es una prueba y se levanto desde");
            } else if (action.equals(Intent.ACTION_USER_PRESENT)) {
                hideDialog();
            } else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
                hideDialog();
            }
        }
    };


}