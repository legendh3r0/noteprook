package com.cloud.notepro.Service;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.UserHandle;
import android.widget.Toast;



public class DeviceAdminSample extends DeviceAdminReceiver {

    void showToast(Context context, String msg) {
        String status = msg;
        Toast.makeText(context, status, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEnabled(Context context, Intent intent) {
        showToast(context, "onEnabled");
    }

    @Override
    public CharSequence onDisableRequested(Context context, Intent intent) {
        return "warning";
    }

    @Override
    public void onDisabled(Context context, Intent intent) {
        showToast(context, "onDisabled");
    }

    @Override
    public void onPasswordChanged(Context context, Intent intent, UserHandle userHandle) {
        showToast(context, "onPasswordChanged");
    }
}
