package com.cloud.notepro;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.EmbossMaskFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.PathEffect;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.cloud.notepro.Helper.DrawHelper;
import com.cloud.notepro.Helper.DrawSupport;
import com.cloud.notepro.Helper.PermissionSupport;
import com.cloud.notepro.Net.MyUrl;
import com.cloud.notepro.Net.SPRSave;
import com.cloud.notepro.bean.DrawObj;
import com.cloud.notepro.bean.PathObj;
import com.cloud.notepro.bean.TypeObj;
import com.google.gson.Gson;
import com.telpoo.frame.database.BaseDBSupport;
import com.telpoo.frame.object.BaseObject;
import com.telpoo.frame.utils.SPRSupport;

import java.util.ArrayList;
import java.util.List;


public class DrawView extends View implements View.OnTouchListener {

    private Canvas mCanvas;
    private Path mPath;
    private Paint mPaint;
    private Paint mPaint2;
    private Paint canvasPaint;
    private ArrayList<PathObj> paths = new ArrayList<PathObj>();
    private ArrayList<BaseObject> pathsPoint = new ArrayList<BaseObject>();
    private ArrayList<BaseObject> listPoint = new ArrayList<BaseObject>();
    private ArrayList<PathObj> undonePaths = new ArrayList<PathObj>();
    PathObj pathObj;
    BaseObject pointMove = new BaseObject();
    Path path;
    static int paintColor = Color.parseColor("#d01716");
    private float STROKE_WIDTH = 5f;
    private boolean erase = false;
    Bitmap canvasBitmap;
    Activity activity;
    Paint.Cap cap = Paint.Cap.ROUND;
    int width = 0;
    float w = 0;
    Bitmap mBitmapBrush;
    private Vector2 mBitmapBrushDimensions;
    private List<Vector2> mPositions = new ArrayList<Vector2>(100);
    Bitmap bitmap;
    Shader shader;
    PathEffect pathEffect;
    BlurMaskFilter mBlur;
    EmbossMaskFilter embossMaskFilter;

    private Bitmap im;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public DrawView(Activity activity) {
        super(activity);
        this.activity = activity;
        setupDrawing();
        setErase(erase);
//        mBitmapBrush = BitmapFactory.decodeResource(activity.getResources(), R.drawable.brush_type_1);
//        getResizedBitmap(mBitmapBrush, 50, 50);
//        mBitmapBrushDimensions = new Vector2(mBitmapBrush.getWidth(), mBitmapBrush.getHeight());
    }

    private static final class Vector2 {
        public Vector2(float x, float y) {
            this.x = x;
            this.y = y;
        }

        public final float x;
        public final float y;
    }


    private void setupDrawing() {
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.setOnTouchListener(this);
        mPaint = new Paint();
        mPaint2 = new Paint();
        mPath = new Path();
        mPaint.setColor(paintColor);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(cap);
        mPaint.setAlpha(2);
//        BlurMaskFilter mBlur = new BlurMaskFilter(200, BlurMaskFilter.Blur.INNER);
//        mPaint.setMaskFilter(mBlur);
        mPaint2.set(mPaint);
        float a = Float.parseFloat("3");
//        mPaint.setMaskFilter(new BlurMaskFilter(15, BlurMaskFilter.Blur.));
        canvasPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        setEffectShader(TypeObj.TYPE_SHADER_LOCAL,null,R.drawable.brush_yellow_fow);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.w = w;

        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ALPHA_8);
        mCanvas = new Canvas(canvasBitmap);
        if (!SPRSave.getShaderRainbow(getContext())) {
            return;
        }
        setEffectRainbow();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //mPath = new Path();
        //canvas.drawPath(mPath, mPaint);
        for (PathObj p : paths) {
            mPaint2.setColor(p.getColor());
            mPaint2.setStrokeWidth(p.getThick());
            mPaint2.setStrokeCap(p.getCap());
            try {
                mPaint2.setShader(p.getShader());
            } catch (Exception e) {
                Log.d("quansp", "Don't have shader");
            }
            canvas.drawPath(p.getPath(), mPaint2);
        }
        canvas.drawPath(mPath, mPaint);
//        for (Vector2 pos : mPositions) {
//            canvas.drawBitmap(mBitmapBrush, pos.x, pos.y, mPaint);
//        }

    }

    public void setErase(boolean isErase) {
        erase = isErase;
//        mPaint = new Paint();
        if (erase) {
            setupDrawing();
            paintColor = Color.WHITE;

//            PorterDuff.Mode mode = PorterDuff.Mode.CLEAR;
//            PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(srcColor, mode);
            setPaintAlpha(SPRSave.getThickEraser(getContext()));
//            mPaint2.setColorFilter(porterDuffColorFilter);
            shader = null;
            mPaint2.setColor(paintColor);
//            mPaint2.setXfermode(new PorterDuffXfermode(mode));
//            mPaint.setColorFilter(porterDuffColorFilter);

            mPaint.setColor(paintColor);
//            mPaint.setXfermode(new PorterDuffXfermode(mode));

        } else {

            setupDrawing();

        }
    }

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

    private void touch_start(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void touch_move(float x, float y) {
//        mPositions.clear();
//        mPositions.add(new Vector2(x - mBitmapBrushDimensions.x / 2, y - mBitmapBrushDimensions.y / 2));
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
    }

    private void touch_up() {
        mPath.lineTo(mX, mY);
        mPath.lineTo(mX, mY);
        // commit the path to our offscreen
        mCanvas.drawPath(mPath, mPaint);
        // kill this so we don't double draw
        pathObj = new PathObj(mPath, paintColor, width, cap, shader);
        paths.add(pathObj);
        mPath = new Path();
    }

    @Override
    public boolean onTouch(View arg0, MotionEvent event) {
        canvasPaint.setColor(paintColor);
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up();
                invalidate();
                if (!PermissionSupport.getInstall(activity).checkRequestStorage(activity)) {
                    Toast.makeText(getContext(), "Cần set quyền mới lưu được !", Toast.LENGTH_LONG).show();
                    return false;
                }
                break;
        }
        return true;
    }

    public void onClickUndo() {
        if (paths.size() > 0) {
            undonePaths.add(paths.remove(paths.size() - 1));
            invalidate();
        } else {

        }
        //toast the user
    }

    public void onClickRedo() {
        if (undonePaths.size() > 0) {
            paths.add(undonePaths.remove(undonePaths.size() - 1));
            invalidate();
        } else {

        }
        //toast the user
    }

    public void setPaintAlpha(int newAlpha) {
        width = newAlpha;
        STROKE_WIDTH = Math.round((float) newAlpha / 100 * 255);
        mPaint.setStrokeWidth(newAlpha);

        if (!SPRSave.getPathEffect(getContext())) {
            return;
        }
        setEffectPath();
    }
    //***************************   respond to touch interaction   **************************************************


    public void setPaintColor(int paintColors) {
        paintColor = paintColors;
        mPaint.setColor(paintColors);
    }

    public void setStrokeCap(Paint.Cap cap) {
        this.cap = cap;
        mPaint.setStrokeCap(cap);
        mPaint2.setStrokeCap(cap);
    }

    public void setEffectPath() {
//        mPaint.setPathEffect(new DashPathEffect(new float[]{10, 50, 1, 50}, 12));
        pathEffect = DrawHelper.getTrianglePathEffect(width);
        mPaint.setPathEffect(pathEffect);
        mPaint2.setPathEffect(pathEffect);
    }

    public void setEffectRainbow() {
        int[] rainbow = DrawHelper.getRainbowColors(getContext());
        shader = new LinearGradient(0, 0, 0, w, rainbow,
                null, Shader.TileMode.MIRROR);
        Matrix matrix = new Matrix();
        matrix.setRotate(90);
        shader.setLocalMatrix(matrix);
        mPaint.setShader(shader);
        mPaint2.setShader(shader);
    }

    public void setEffectShader(int type, String path, int id) {
        switch (type) {
            case TypeObj.TYPE_SHADER_LOCAL:
                bitmap = BitmapFactory.decodeResource(
                        getResources(), id);
                break;
            case TypeObj.TYPE_SHADER_PHONE:
                bitmap = BitmapFactory.decodeFile(path);
                break;
        }
        shader = new BitmapShader(bitmap,
                Shader.TileMode.MIRROR, Shader.TileMode.MIRROR);
        mPaint.setShader(shader);
        mPaint2.setShader(shader);
    }

    public void setEffectMaskFilter(int type) {
        BlurMaskFilter.Blur blur = null;
        switch (type) {
            case TypeObj.TYPE_BLUR_MASK:
                switch (SPRSave.getTypeBlurMask(getContext())) {
                    case TypeObj.INNER:
                        blur = BlurMaskFilter.Blur.INNER;
                        break;
                    case TypeObj.OUTER:
                        blur = BlurMaskFilter.Blur.OUTER;
                        break;
                    case TypeObj.SOLID:
                        blur = BlurMaskFilter.Blur.SOLID;
                        break;
                    case TypeObj.NORMAL:
                        blur = BlurMaskFilter.Blur.NORMAL;
                        break;
                }
                mBlur = new BlurMaskFilter(200, blur);
                mPaint.setMaskFilter(mBlur);
                break;
            case TypeObj.TYPE_EMBOSS_MASK:
                embossMaskFilter = new EmbossMaskFilter(
                        new float[]{1,5,1}, // direction of the light source
                        0.5f, // ambient light between 0 to 1
                        10, // specular highlights
                        7.5f // blur before applying lighting
                );
                mPaint.setMaskFilter(embossMaskFilter);
                break;
        }
    }


}

