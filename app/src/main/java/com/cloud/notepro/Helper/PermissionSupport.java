package com.cloud.notepro.Helper;

import android.Manifest;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.cloud.notepro.Service.DeviceAdminSample;
import com.cloud.notepro.bean.TypeObj;

public class PermissionSupport {

    private static Activity activity;
    private static PermissionSupport support;
    public static final int REQUEST_STORAGE_PERMISSIONS = 1;
    public static DevicePolicyManager devicePolicyManager;


    public PermissionSupport(Activity activity) {
        this.activity = activity;
    }

    public static PermissionSupport getInstall(Activity activity) {
        if (support == null || activity != support.activity)
            support = new PermissionSupport(activity);
        return support;
    }

    public static boolean hasPermissionAdminGrandted() {
        devicePolicyManager = (DevicePolicyManager) activity.getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (!devicePolicyManager.isAdminActive(new ComponentName(activity, DeviceAdminSample.class))) {
            ComponentName deviceAdminComponentName = new ComponentName(activity, DeviceAdminSample.class);
            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, deviceAdminComponentName);
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "You have to granded this permission to set Wallpaper.");
            activity.startActivityForResult(intent, TypeObj.REQUEST_ADMIN_PERMISSIONS);
            return false;
        }
        return true;
    }

    public static Boolean hasPermissionGranted(String permission) {
        Boolean status = ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
        Log.d("PermissionSupport", "hasPermissionGranted: " + permission + ": " + status);
        return status;
    }

    public static Boolean hasPermissionGranted(Context context, String permission) {
        Boolean status = ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
        Log.d("PermissionSupport", "hasPermissionGranted: " + permission + ": " + status);
        return status;
    }

    public static Boolean requestPermissionStore(int requestCode) {
        if (hasPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE))
            return true;
        if (Build.VERSION.SDK_INT < 23) return true;
        activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
        return false;
    }

    public static boolean isPermission(Context context, int requestCode, String perMission) {
        if (ContextCompat.checkSelfPermission(context, perMission) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{perMission}, requestCode);
            return false;
        }
        return true;
    }


    public static boolean checkRequestStorage(Activity context) {
        if (hasPermissionGranted(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                && hasPermissionGranted(context, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            return true;

        ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSIONS);
        return false;
    }


}
