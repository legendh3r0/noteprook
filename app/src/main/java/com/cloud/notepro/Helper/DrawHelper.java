package com.cloud.notepro.Helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.PathEffect;

import com.cloud.notepro.R;

public class DrawHelper {

    //For rainbow effect
    public static int[] getRainbowColors(Context context) {
        return new int[] {
                context.getResources().getColor(R.color.red_500),
                context.getResources().getColor(R.color.yellow_500),
                context.getResources().getColor(R.color.green_500),
                context.getResources().getColor(R.color.blue_500),
                context.getResources().getColor(R.color.purple_500)
        };
    }

    //For PathEffect
    public static PathEffect getTrianglePathEffect(int strokeWidth) {
        return new PathDashPathEffect(
                getTriangle(strokeWidth),
                strokeWidth,
                0.0f,
                PathDashPathEffect.Style.ROTATE);
    }

    public static Path getTriangle(float size) {
        Path path = new Path();
//        float half = size / 2;
//        path.moveTo(-half, -half);
//        path.lineTo(half, -half);
//        path.lineTo(0, half);
//        path.close();
        path.addCircle(size,size,size, Path.Direction.CCW);
        return path;
    }

    //For Image Brush effect type 1
    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }
}
