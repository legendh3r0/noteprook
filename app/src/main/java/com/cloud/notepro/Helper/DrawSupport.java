package com.cloud.notepro.Helper;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.cloud.notepro.Net.MyUrl;

import java.io.File;
import java.io.FileOutputStream;

public class DrawSupport {

    public static void drawCanvas(View v, String path) {
        try {
            Bitmap workingBitmap = BitmapFactory.decodeFile(path);

            Drawable drawable = Drawable.createFromPath(path);
            Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            canvas.save();
            canvas.translate(10, 10);
//            v.setBackground(drawable);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            v.setBackground(drawable);
            canvas.restore();
        }
        catch (Exception e) {
            Log.d("quanerr", "Canvas draw : " + e.getMessage());
        }
    }


    public static void saveImagDrawPic(View v, String name, Context context) {
        String namePath = MyUrl.path + "/" + name + ".jpg";
        View content = v;
        content.setDrawingCacheEnabled(true);
        content.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        Bitmap bitmap = content.getDrawingCache();
        File dir = new File(MyUrl.path);
        dir.mkdir();
        File file = new File(MyUrl.path + "/" + name + ".jpg");
        FileOutputStream ostream;
        try {
            file.delete();
            file.createNewFile();
            ostream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, ostream);
            ostream.flush();
            ostream.close();
            drawCanvas(v, namePath);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "error", Toast.LENGTH_LONG).show();
        }
    }

    public static class MyAsyncTask extends AsyncTask<Void, Integer, Void> {

        View v;
        String name;
        Context context;

        public MyAsyncTask(View v, String name, Context context) {
            this.v = v;
            this.name = name;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Hàm này sẽ chạy đầu tiên khi AsyncTask này được gọi
            //Ở đây mình sẽ thông báo quá trình load bắt đâu "Start"
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected Void doInBackground(Void... params) {
            //Hàm được được hiện tiếp sau hàm onPreExecute()
            //Hàm này thực hiện các tác vụ chạy ngầm
            //Tuyệt đối k vẽ giao diện trong hàm này
            saveImagDrawPic(v,name,context);
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            //Hàm thực hiện update giao diện khi có dữ liệu từ hàm doInBackground gửi xuống
            super.onProgressUpdate(values);
            //Thông qua contextCha để lấy được control trong MainActivity
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Hàm này được thực hiện khi tiến trình kết thúc
            //Ở đây mình thông báo là đã "Finshed" để người dùng biết
        }
    }

}
