package com.cloud.notepro;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.cloud.notepro.Helper.DrawSupport;
import com.cloud.notepro.Helper.PermissionSupport;
import com.cloud.notepro.Net.MyUrl;
import com.cloud.notepro.Net.SPRSave;
import com.cloud.notepro.Service.NoteForegroundService;
import com.cloud.notepro.bean.TypeObj;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.telpoo.frame.utils.SPRSupport;

import java.lang.reflect.Type;

public class MainActivity extends AppCompatActivity {

    SeekBar seekBar;
    RelativeLayout colorPicker;
    DrawView drawView;
    LinearLayout mainViewDraw;
    ImageView imgPen, imgPencil, imgPenMaker, imgEraser, imgUndo, imgRedo;
    String namePathMain = "";
    int thick = 10;
    int color = 0;
    DrawSupport.MyAsyncTask myAsyncTask;
    Thread threadSaveImg;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.activity_main);
        Intent i = new Intent(MainActivity.this, ImageCropActivity.class);
        startActivity(i);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, 1);
        }
        initView();
        initData();
    }

    public void initView() {
        imgPen = findViewById(R.id.imgPen);
        imgPencil = findViewById(R.id.imgPencil);
        imgPenMaker = findViewById(R.id.imgPenMaker);
        imgEraser = findViewById(R.id.imgEraser);
        mainViewDraw = findViewById(R.id.mainViewDraw);
        seekBar = findViewById(R.id.seekBar);
        colorPicker = findViewById(R.id.colorPicker);
        imgUndo = findViewById(R.id.imgUndo);
        imgRedo = findViewById(R.id.imgRedo);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void initData() {
        drawView = new DrawView(MainActivity.this);
        drawView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mainViewDraw.addView(drawView);
        switch (SPRSave.getTypePen(MainActivity.this)) {
            case TypeObj.TYPE_PEN:
                thick = SPRSave.getThickPen(MainActivity.this);
                color = SPRSave.getColorPen(MainActivity.this);
                break;
            case TypeObj.TYPE_PENCIL:
                thick = SPRSave.getThickPencil(MainActivity.this);
                color = SPRSave.getColorPencil(MainActivity.this);
                break;
            case TypeObj.TYPE_MAKER:
                thick = SPRSave.getThickPenMaker(MainActivity.this);
                color = SPRSave.getColorMaker(MainActivity.this);
//                drawView.setStrokeCap(Paint.Cap.BUTT);
                break;
            case TypeObj.TYPE_ERASER:
                thick = SPRSave.getThickEraser(MainActivity.this);
                break;
        }
        seekBar.setMax(200);
        setSeekBar(color);
        seekBar.setProgress(thick);
        colorPicker.getBackground().setTint(color);
        drawView.setDrawingCacheEnabled(true);
        drawView.setEnabled(true);
        drawView.setPaintColor(color);
        drawView.setPaintAlpha(thick);
        startService(new Intent(MainActivity.this, NoteForegroundService.class));
        namePathMain = MyUrl.path + "/" + getResources().getString(R.string.name_save_image) + ".jpg";
        DrawSupport.drawCanvas(drawView, namePathMain);
        buttonClick();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void buttonClick(){
        imgUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawView.onClickUndo();
            }
        });
        imgRedo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawView.onClickRedo();
            }
        });
        imgPen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SPRSave.saveTypePen(TypeObj.TYPE_PEN, MainActivity.this);
                SPRSave.saveIsEraser(false, MainActivity.this);
                thick = SPRSave.getThickPen(MainActivity.this);
                color = SPRSave.getColorPen(MainActivity.this);
                setDisplay(Paint.Cap.ROUND);

            }
        });
        imgPencil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SPRSave.saveTypePen(TypeObj.TYPE_PENCIL, MainActivity.this);
                SPRSave.saveIsEraser(false, MainActivity.this);
                thick = SPRSave.getThickPencil(MainActivity.this);
                color = SPRSave.getColorPencil(MainActivity.this);
                setDisplay(Paint.Cap.ROUND);

            }
        });
        imgPenMaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SPRSave.saveTypePen(TypeObj.TYPE_MAKER, MainActivity.this);
                SPRSave.saveIsEraser(false, MainActivity.this);
                thick = SPRSave.getThickPenMaker(MainActivity.this);
                color = SPRSave.getColorMaker(MainActivity.this);
                setDisplay(Paint.Cap.BUTT);
            }
        });
        colorPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                colorPickerBuild(color, SPRSave.getTypePen(MainActivity.this));
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                switch (SPRSave.getTypePen(MainActivity.this)) {
                    case TypeObj.TYPE_PEN:
                        SPRSave.saveThickPen(i, MainActivity.this);
                        drawView.setPaintAlpha(i);
                        break;
                    case TypeObj.TYPE_PENCIL:
                        SPRSave.saveThickPencil(i, MainActivity.this);
                        drawView.setPaintAlpha(i);
                        break;
                    case TypeObj.TYPE_MAKER:
                        SPRSave.saveThickPenMaker(i, MainActivity.this);
                        drawView.setPaintAlpha(i);
                        break;
                    case TypeObj.TYPE_ERASER:
                        SPRSave.saveThickEraser(i, MainActivity.this);
                        drawView.setPaintAlpha(i);
                        break;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        imgEraser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SPRSave.saveTypePen(TypeObj.TYPE_ERASER, MainActivity.this);
                SPRSave.saveIsEraser(true, MainActivity.this);
                thick = SPRSave.getThickEraser(MainActivity.this);
                seekBar.setProgress(thick);
                colorPicker.setVisibility(View.GONE);
                drawView.setErase(true);
                drawView.setStrokeCap(Paint.Cap.ROUND);
                setSeekBar(Color.BLACK);
            }
        });
//        drawView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                if (!PermissionSupport.getInstall(MainActivity.this).checkRequestStorage(MainActivity.this)) {
//                    Toast.makeText(MainActivity.this, "Cần set quyền mới lưu được !", Toast.LENGTH_LONG).show();
//                    return false;
//                }
//                if (SPRSave.getIsEraser(MainActivity.this) && SPRSave.getEraserAll(MainActivity.this)) {
//                    drawView.setBackgroundColor(Color.TRANSPARENT);
//                }
//                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
//                    DrawSupport.saveImagDrawPic(drawView, getResources().getString(R.string.name_save_image), MainActivity.this);
////                    saveImgThread();
////                    myAsyncTask = new DrawSupport.MyAsyncTask(drawView, getResources().getString(R.string.name_save_image), MainActivity.this);
////                    myAsyncTask.execute();
////                    DrawSupport.saveImagDrawPic(drawView, getResources().getString(R.string.name_save_image), MainActivity.this);
//                }
//                return false;
//            }
//        });
    }

    public void saveImgThread() {
        Thread threadSaveImg = new Thread() {
            @Override
            public void run() {
                DrawSupport.saveImagDrawPic(drawView, getResources().getString(R.string.name_save_image), MainActivity.this);
            }
        };
        threadSaveImg.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void colorPickerBuild(int color, final int type) {
        ColorPickerDialogBuilder
                .with(MainActivity.this)
                .setTitle("Choose color")
                .initialColor(color)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .showAlphaSlider(type == TypeObj.TYPE_MAKER ? false : true)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int selectedColor) {

                    }
                })
                .setPositiveButton("ok", new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        switch (type) {
                            case TypeObj.TYPE_PEN:
                                SPRSave.saveColorPen(selectedColor, MainActivity.this);
                                colorPicker.getBackground().setTint(selectedColor);
                                drawView.setPaintColor(selectedColor);
                                break;
                            case TypeObj.TYPE_PENCIL:
                                SPRSave.saveColorPencil(selectedColor, MainActivity.this);
                                colorPicker.getBackground().setTint(selectedColor);
                                drawView.setPaintColor(selectedColor);
                                break;
                            case TypeObj.TYPE_MAKER:
                                SPRSave.saveColorMaker(selectedColor, MainActivity.this);
                                colorPicker.getBackground().setTint(selectedColor);
                                drawView.setPaintColor(selectedColor);
                                break;
                        }
                        setSeekBar(selectedColor);
                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void setDisplay(Paint.Cap cap) {
        colorPicker.getBackground().setTint(color);
        colorPicker.setVisibility(View.VISIBLE);
        seekBar.setProgress(thick);
        drawView.setErase(SPRSave.getIsEraser(MainActivity.this));
        drawView.setPaintColor(color);
        drawView.setPaintAlpha(thick);
        drawView.setStrokeCap(cap);
        setSeekBar(color);
    }

    public void setSeekBar(int color) {
        seekBar.getThumb().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        seekBar.getProgressDrawable().setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        seekBar.getProgressDrawable().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
    }


    @Override
    protected void onPause() {
        super.onPause();
        DrawSupport.saveImagDrawPic(drawView, getResources().getString(R.string.name_save_image), MainActivity.this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DrawSupport.saveImagDrawPic(drawView, getResources().getString(R.string.name_save_image), MainActivity.this);
    }
}