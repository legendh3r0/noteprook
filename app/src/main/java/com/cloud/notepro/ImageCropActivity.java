package com.cloud.notepro;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.yalantis.ucrop.UCrop;

import java.io.File;

import static com.cloud.notepro.bean.CropImageObj.*;

public class ImageCropActivity extends AppCompatActivity {

    ImageView imgView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_crop);

        initView();
        initData();
    }

    public void initView() {
        imgView = findViewById(R.id.imgView);
    }

    public void initData() {
        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent().setAction(Intent.ACTION_GET_CONTENT).setType("image/*"), CODE_IMG_GALLERY);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE_IMG_GALLERY && resultCode == RESULT_OK) {
            //DESDE GALLERY
            Uri imgUri = data.getData();
            if (imgUri != null) {
                startCrop(imgUri);
            }
        }
        else if (requestCode == REQUEST_CROP && resultCode == RESULT_OK) {
            Uri imgResultCrop = UCrop.getOutput(data);
            if (imgResultCrop != null) {
                imgView.setImageURI(imgResultCrop);
            }
        }
    }

    public void startCrop(@NonNull Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMG_NAME;
        destinationFileName += ".jpg";
        String i = getExternalCacheDir().getPath();
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getExternalCacheDir(), destinationFileName)));
        uCrop.withAspectRatio(1,1);
//        uCrop.withAspectRatio(3,4);
//        uCrop.useSourceImageAspectRatio();
//        uCrop.withAspectRatio(2,3);
//        uCrop.withAspectRatio(16,9);
//        uCrop.withMaxResultSize(450,450);
        uCrop.withOptions(getCropOption());
        uCrop.start(ImageCropActivity.this);
    }

    private UCrop.Options getCropOption() {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionQuality(70);

        //Compress type
//        options.setCompressionFormat(Bitmap.CompressFormat.PNG);
//        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);

        //UI
        options.setHideBottomControls(false);
        options.setFreeStyleCropEnabled(true);

        //Color
        options.setStatusBarColor(getResources().getColor(R.color.red_200));
        options.setToolbarColor(getResources().getColor(R.color.yellow_500));
        options.setToolbarTitle("Crop image");
        return options;
    }
}