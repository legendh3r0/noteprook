package com.cloud.notepro;

import android.app.Application;

import com.cloud.notepro.Net.SPRSave;
import com.cloud.notepro.bean.DrawObj;
import com.telpoo.frame.database.BaseDBSupport;


public class MyApplication extends Application {
    private static MyApplication mInstance;

    String[] tableName = new String[1];
    String[][] column = new String[1][2];

    @Override
    public void onCreate() {
        super.onCreate();
        tableName[0] = getResources().getString(R.string.table_string);
        column[0][0] = DrawObj.move_x;
        column[0][1] = DrawObj.move_y;
        BaseDBSupport.init(tableName, column, this, getResources().getString(R.string.db_string), 2);
        SPRSave.creatApp(this);
    }


    public static synchronized MyApplication getInstance() {
        return mInstance;
    }
}
