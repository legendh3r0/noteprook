package com.cloud.notepro.bean;

import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;


public class PathObj {

    public Path path;
    public int color;
    public int thick;
    public Paint.Cap cap;
    public Shader shader;

    public PathObj() {

    }

    public PathObj(Path path, int color, int thick, Paint.Cap cap, Shader shader) {
        this.path = path;
        this.color = color;
        this.thick = thick;
        this.cap = cap;
        this.shader = shader;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getThick() {
        return thick;
    }

    public void setThick(int thick) {
        this.thick = thick;
    }

    public Paint.Cap getCap() {
        return cap;
    }

    public void setCap(Paint.Cap cap) {
        this.cap = cap;
    }

    public Shader getShader() {
        return shader;
    }

    public void setShader(Shader shader) {
        this.shader = shader;
    }
}
