package com.cloud.notepro.bean;

public class TypeObj {

    public static int REQUEST_ADMIN_PERMISSIONS = 1;
    public final static int TYPE_PEN = 2;
    public final static int TYPE_PENCIL = 3;
    public final static int TYPE_MAKER = 4;
    public final static int TYPE_ERASER = 5;
    public final static int TYPE_SHADER_LOCAL = 6;
    public final static int TYPE_SHADER_PHONE = 7;
    public final static int TYPE_EMBOSS_MASK = 8;
    public final static int TYPE_BLUR_MASK = 9;

    //TYPE MASK FILTER
    public final static int INNER = 10;
    public final static int SOLID = 11;
    public final static int OUTER = 12;
    public final static int NORMAL = 13;
}
