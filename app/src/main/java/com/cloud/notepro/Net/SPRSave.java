package com.cloud.notepro.Net;

import android.content.Context;
import android.graphics.Color;

import com.cloud.notepro.bean.TypeObj;
import com.telpoo.frame.utils.SPRSupport;

import java.lang.reflect.Type;

public class SPRSave {
    public static String type_pen = "type_pen";
    public static String thick_pen = "thick_pen";
    public static String thick_pencil = "thick_pencil";
    public static String thick_pen_maker = "thick_pen_maker";
    public static String thick_eraser = "thick_eraser";
    public static String color_pen = "color_pen";
    public static String color_pencil = "color_pencil";
    public static String color_pen_maker = "color_pen_maker";
    public static String is_eraser = "is_eraser";
    public static String eraser_all = "eraser_all";
    public static String shader_rainbow = "shader_rainbow";
    public static String path_effect = "path_effect";
    public static String type_blur_mask_filter = "type_blur_mask_filter";

    public static void saveTypePen(int type, Context context) {
        SPRSupport.save(type_pen, type, context);
    }
    public static int getTypePen(Context context) {
        return SPRSupport.getInt(type_pen, context);
    }

    public static void saveThickPen(int thick, Context context) {
        SPRSupport.save(thick_pen, thick, context);
    }
    public static int getThickPen(Context context) {
        return SPRSupport.getInt(thick_pen, context);
    }

    public static void saveThickPencil(int thick, Context context) {
        SPRSupport.save(thick_pencil, thick, context);
    }
    public static int getThickPencil(Context context) {
        return SPRSupport.getInt(thick_pencil, context);
    }

    public static void saveThickPenMaker(int thick, Context context) {
        SPRSupport.save(thick_pen_maker, thick, context);
    }
    public static int getThickPenMaker(Context context) {
        return SPRSupport.getInt(thick_pen_maker, context);
    }

    public static void saveThickEraser(int thick, Context context) {
        SPRSupport.save(thick_eraser, thick, context);
    }
    public static int getThickEraser(Context context) {
        return SPRSupport.getInt(thick_eraser, context);
    }

    public static void saveColorPen(int color, Context context) {
        SPRSupport.save(color_pen, color, context);
    }
    public static int getColorPen(Context context) {
        return SPRSupport.getInt(color_pen, context);
    }

    public static void saveColorPencil(int color, Context context) {
        SPRSupport.save(color_pencil, color, context);
    }
    public static int getColorPencil(Context context) {
        return SPRSupport.getInt(color_pencil, context);
    }

    public static void saveColorMaker(int color, Context context) {
        SPRSupport.save(color_pen_maker, color, context);
    }
    public static int getColorMaker(Context context) {
        return SPRSupport.getInt(color_pen_maker, context);
    }

    public static void saveIsEraser(boolean isEraser, Context context) {
        SPRSupport.save(is_eraser, isEraser, context);
    }
    public static boolean getIsEraser(Context context) {
        boolean c = SPRSupport.getBool(is_eraser, context);
        return SPRSupport.getBool(is_eraser, context);
    }

    public static void saveEraserAll(boolean eraserAll, Context context) {
        SPRSupport.save(eraser_all, eraserAll, context);
    }
    public static boolean getEraserAll(Context context) {
        return SPRSupport.getBool(eraser_all, context);
    }

    public static void saveShaderRainbow(boolean shaderRainbow, Context context) {
        SPRSupport.save(shader_rainbow, shaderRainbow, context);
    }
    public static boolean getShaderRainbow(Context context) {
        return SPRSupport.getBool(shader_rainbow, context);
    }

    public static void savePathEffect(boolean pathEffect, Context context) {
        SPRSupport.save(path_effect, pathEffect, context);
    }
    public static boolean getPathEffect(Context context) {
        return SPRSupport.getBool(path_effect, context);
    }

    public static void saveTypeBlurMask(int typeBlur, Context context) {
        SPRSupport.save(type_blur_mask_filter, typeBlur, context);
    }
    public static int getTypeBlurMask(Context context) {
        return SPRSupport.getInt(type_blur_mask_filter, context);
    }

    public static void creatApp(Context context) {
        SPRSave.saveTypePen(TypeObj.TYPE_PEN, context);
        if (SPRSave.getThickPen(context) < 0) {
            SPRSave.saveThickPen(3, context);
        }
        if (SPRSave.getThickPencil(context) < 0) {
            SPRSave.saveThickPencil(3, context);
        }
        if (SPRSave.getThickPenMaker(context) < 0) {
            SPRSave.saveThickPenMaker(15, context);
        }
        if (SPRSave.getThickEraser(context) < 0) {
            SPRSave.saveThickEraser(200, context);
        }
        if (SPRSave.getColorPen(context) == -2147483648) {
            SPRSave.saveColorPen(Color.parseColor("#000000"), context);
        }
        if (SPRSave.getColorPencil(context) == -2147483648) {
            SPRSave.saveColorPencil(Color.parseColor("#000000"), context);
        }
        if (SPRSave.getColorMaker(context) == -2147483648) {
            SPRSave.saveColorMaker(Color.parseColor("#9EFFEE58"), context);
        }
        SPRSave.saveIsEraser(false, context);
    }
}
